CREATE TABLE application1 (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  login varchar(30) NOT NULL DEFAULT '',
  pass varchar(100) NOT NULL DEFAULT '',
  name varchar(30) NOT NULL DEFAULT '',
  email varchar(128) NOT NULL DEFAULT '',
  year int(10) NOT NULL DEFAULT 0,
  abilities varchar(30) NOT NULL DEFAULT '',
  gender varchar(6) NOT NULL DEFAULT 0,
  limbs tinyint(1) NOT NULL DEFAULT 0,
  bio varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
);