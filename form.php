<!DOCTYPE html>
<html lang="ru">
  <head>
  <link rel=stylesheet href="css.css">
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
div{
color: white;
}
p {
 border: 2px solid red;
 }
<?php if ($errors['bio']) {print '.form-inner textarea {border: 2px solid red; }';} ?>
    </style>
  </head>

  <body>

<form class="transparent" action="" method="POST">
  <div class="form-inner">
  <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
  print($message);
  }
  print('</div>');
}
?>
<br />
<div>
  <label>Enter the name:</label>
  <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
  </div>
  <br />
  <label>Enter the email:</label>
  <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" />
  <br />
  <label>Select Your year of birth:</label>
  <br />
<select name="year[]" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>">
<?php
foreach ($year as $key => $value){
$selected = empty($values['year'][$key]) ? '' : 'selected="selected"';
printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
}
?>
</select>
  <br />
  <br />
  <label><?php if ($errors['gender']) {print '<p>';} ?>Your gender:<?php if ($errors['gender']) {print '</p>';} ?></label>
  <br />
  <text><input type="radio"
        name="gender" value="Female" <?php if ($values['gender']=="Female") {print 'checked="checked"';} ?>/>
        Female</text>
        <text><input type="radio"
        name="gender" value="Male" <?php if ($values['gender']=="Male") {print 'checked="checked"';} ?>/>
        Male</text>
        <br />
      <label><?php if ($errors['limbs']) {print '<p>';} ?>Your amount of limbs:<?php if ($errors['limbs']) {print '</p>';} ?></label>
      <text><input type="radio"
        name="limbs" value="1" 
        <?php if ($values['limbs']=="1") {print 'checked="checked"';} ?>/>
      1</text>
      <text><input type="radio"
        name="limbs" value="2"
        <?php if ($values['limbs']=="2") {print 'checked="checked"';} ?> />
      2</text>
      <text><input type="radio" 
        name="limbs" value="3" 
        <?php if ($values['limbs']=="3") {print 'checked="checked"';} ?>/>
      3</text>
      <text><input type="radio"
        name="limbs" value="4" 
        <?php if ($values['limbs']=="4") {print 'checked="checked"';} ?>/>
      4</text>
      <text><input type="radio" 
        name="limbs" value="More"
        <?php if ($values['limbs']=="More") {print 'checked="checked"';} ?> />
      More</text>
        <br />
        <br />
      <label>Select Your abilitie(s):</label>
      <br />
      <select name="abilities[]" multiple="multiple" <?php if ($errors['abilities']) {print 'class="error"';} ?> value="<?php print $values['abilities']; ?>">
<?php
foreach ($abilities as $key => $value){
$selected = empty($values['abilities'][$key]) ? '' : 'selected="selected"';
printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
}
?>
</select>
  <br />
  <br />
  <div>
<label>Biography:</label>

  <textarea name="bio"><?php print($values['bio']); ?></textarea>
  </div>
  
  <br />
  <br />
  <input name="accept" type="checkbox" id="custom-checkbox" checked/>
  <label for="custom-checkbox">I accept with contract</label>
  <br />
  <input name="submit" type="submit" />
</div>
</form>
</body>
</html>