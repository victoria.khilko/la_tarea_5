<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['Immortality'=>'Immortality','Passing through walls'=>'Passing through walls','Levitation'=>'Levitation'];
for ($i=1900;$i<=2020;$i++){
    $year[$i] = $i;
}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Thanks, results saved.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages[] = sprintf(' You can <a href="login.php">enter</a> with login <strong>%s</strong>
      and pass <strong>%s</strong> for change databases.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);

  // TODO: аналогично все поля.
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['bio'] = !empty($_COOKIE['bio_error']);
  $errors['accept'] = !empty($_COOKIE['accept_error']);

  // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '',100000);
        // Выводим сообщение.
        if ($_COOKIE['fio_error'] == '1'){
            $messages[] = '<div class="error">No name.</div>';
        }
        if ($_COOKIE['fio_error'] == '2'){ $messages[] = '<div class="error">Name is word made from english letters.</div>';
        }
    }
    // TODO: тут выдать сообщения об ошибках в других полях.
    
    if ($errors['email']) {
        setcookie('email_error', '',100000);
        
        if ($_COOKIE['email_error'] == '1'){
            $messages[] = '<div class="error">No email.</div>';
        }
        if ($_COOKIE['email_error'] == '2'){ $messages[] = '<div class="error">Email is word how us1er@mysite.com and other.</div>';
        }
    }
    
    if ($errors['year']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('year_error', '', 100000);
        // Выводим сообщение.
        if ($_COOKIE['year_error'] == '1'){
            $messages[] = '<div class="error">Enter year.</div>';
        }
    }
    
    
    if ($errors['gender']) {
        setcookie('gender_error', '',100000);
        $messages[] = '<div class="error">No gender.</div>';
    }
    if ($errors['limbs']) {
        setcookie('limbs_error', '',100000);
        $messages[] = '<div class="error">No limbs.</div>';
    }
    if ($errors['abilities']) {
        setcookie('abilities_error', '',100000);
        $messages[] = '<div class="error">No abilities.</div>';
    }
    
    if ($errors['bio']) {
        setcookie('bio_error', '',100000);
        
        if ($_COOKIE['bio_error'] == '1'){
            $messages[] = '<div class="error">No bio.</div>';
        }
        if ($_COOKIE['bio_error'] == '2'){ $messages[] = '<div class="error">Bio is text only with a-z A-Z.</div>';
        }
    }
    
    if ($errors['accept']) {
        setcookie('accept_error', '',100000);
        $messages[] = '<div class="error">No accept.</div>';
    }

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  //$values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.

  $values['fio'] = (empty($_COOKIE['fio_value']) || !preg_match('/^[a-zA-Z]/',$_COOKIE['fio_value']) ) ? '' : $_COOKIE['fio_value'];                  //dhfhbghjsg dgbwsjjvag fsvdazsdafh adyaguyvnanvdusbgtvfbjsdhybnfhgkjdbfsjxdbnkdh
  // TODO: аналогично все поля.
  $values['email'] = (empty($_COOKIE['email_value']) || !preg_match('/^[a-zA-Z0-9_\-.]+@[a-z.]/', $_COOKIE['email_value']) ) ? '' : $_COOKIE['email_value'];
  $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
  $values['bio'] = (empty($_COOKIE['bio_value']) || !preg_match('/^[a-zA-Z ]/',$_COOKIE['bio_value'])) ? '' : $_COOKIE['bio_value'];
  $values['accept'] = empty($_COOKIE['accept_value']) ? '' : $_COOKIE['accept_value'];
  
  if(!empty($_COOKIE['year_value'])){
      $year_value = json_decode($_COOKIE['year_value']);
  }
  $values['year'] = [];
  if(isset($year_value) && is_array($year_value)){
      foreach ($year_value as $year1){
          if(!empty($year[$year1])){
              $values['year'][$year1] = $year1;
          }
      }
  }
  if (!empty($_COOKIE['abilities_value'])){
    $abilities_value = json_decode($_COOKIE['abilities_value']);
}
$values['abilities'] = [];
if (isset($abilities_value) && is_array($abilities_value)){
    foreach ($abilities_value as $ability){
        if (!empty($abilities[$ability]))
        {
            $values['abilities'][$ability] = $ability;
        }
    }
}

  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД
    // и заполнить переменную $values,
    // предварительно санитизовав.
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    printf('Enter with login %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
      // Выдаем куку с флажком об ошибке в поле fio.
      setcookie('fio_error', '1');
      $errors = TRUE;
  }
  else {
      if (!preg_match('/^[a-zA-Z]/', $_POST['fio'])) {
          setcookie('fio_error', '2');
          $errors = TRUE;
      }
      
      else{
          
          // Сохраняем ранее введенное в форму значение на год.
          setcookie('fio_value', $_POST['fio'], time() + 60 * 60 * 24 * 365);
      }
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************

if (empty($_POST['email'])) {
    setcookie('email_error', '1');
    $errors = TRUE;
}
else {
    
    if (!preg_match('/^[a-zA-Z0-9_\-.]+@[a-z.]/', $_POST['email'])) {
        setcookie('email_error', '2');
        $errors = TRUE;
    }
    
    else{
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('email_value', $_POST['email'], time() + 60 * 60 * 24 * 365);
    }
}

if (empty($_POST['year'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('year_error', '1');
    $errors = TRUE;
}
else {
    $year_error = FALSE;
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('year_value', json_encode($_POST['year']), time() + 12 * 30 * 24 * 60 * 60);
}

if (empty($_POST['gender'])) {
    // Выдаем куку с флажком об ошибке в поле fio.
    setcookie('gender_error', '1');
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('gender_value', $_POST['gender'], time() + 60 * 60 * 24 * 365);
}
if (empty($_POST['limbs'])) {
    // Выдаем куку с флажком об ошибке в поле fio.
    setcookie('limbs_error', '1');
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('limbs_value', $_POST['limbs'], time() + 60 * 60 * 24 * 365);
}

if (empty($_POST['abilities'])) {
    // Выдаем куку с флажком об ошибке в поле fio.
    setcookie('abilities_error', '1');
    $errors = TRUE;
}
else {
    $abilities_error = FALSE;
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('abilities_value', json_encode($_POST['abilities']), time() + 60 * 60 * 24 * 365);
}

if (empty($_POST['bio'])) {
    setcookie('bio_error', '1');
    $errors = TRUE;
}
else {
    if (!preg_match('/^[a-zA-Z ]/', $_POST['bio'])) {
        setcookie('bio_error', '2');
        $errors = TRUE;
    }
    else{
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('bio_value', $_POST['bio'], time() + 60 * 60 * 24 * 365);
    }
}

if (empty($_POST['accept'])) {
    // Выдаем куку с флажком об ошибке в поле fio.
    setcookie('accept_error', '1');
    $errors = TRUE;
}
else {
    // Сохраняем ранее введенное в форму значение на год.
    setcookie('accept_value', $_POST['accept'], time() + 60 * 60 * 24 * 365);
}




  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('bio_error', '', 100000);
  }

  $year_bd = array();
  foreach ($_POST['year'] as $key => $value) {
      $year_bd[$key] = $value;
  }
  $year_string = implode(', ', $year_bd);
  
  $a = array();
  foreach ($_POST['abilities'] as $key => $value) {
      $a[$key] = $value;
  }
  $ab = implode(', ', $a);

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    $user = 'u16351';
    $pass = '7947569';
    $db = new PDO('mysql:host=localhost;dbname=u16351', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $log=strip_tags($_SESSION['login']);
    $pas=$_SESSION['pass'];
    // Подготовленный запрос. Не именованные метки.
    $stmt = $db->prepare("UPDATE application1 SET name = ?, email = ?, year = ?, abilities = ?, gender = ?, limbs = ?, bio = ? WHERE login = ? AND pass = ?");
    $stmt->execute(array($_POST['fio'],$_POST['email'],$year_string,$ab,$_POST['gender'],$_POST['limbs'],$_POST['bio'], $log, $pas));

  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = rand();
    $pass = rand();
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    // ...

    $user = 'u16351';
    $passme = '7947569';
    $db = new PDO('mysql:host=localhost;dbname=u16351', $user, $passme, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    $stmt = $db->prepare("INSERT INTO application1 SET login = ?, pass = ?, name = ?, email = ?, year = ?, abilities = ?, gender = ?, limbs = ?, bio = ?");
    $stmt -> execute(array($login, md5($pass), $_POST['fio'],$_POST['email'],$year_string,$ab,$_POST['gender'],$_POST['limbs'],$_POST['bio']));

  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}